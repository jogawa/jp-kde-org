---
title: GUI Building with KDE
summary: This is an introduction to creating GUIs with KDE.
layout: archive
---

<H3>Richard J. Moore</H3>
<HR>
<H2>
Introduction</H2>
Creating GUIs with the KDE Desktop Environment is very easy thanks to the
large number of preexisting GUI components that are available for you to
use, and to the framework Qt provides for developers. Unfortunately while
there is a large body of public domain source code (for example the entire
KDE), and detailed class documentation to learn from, there has not been
a tutorial that explains how to put things together to create a GUI. The
aim of this document is to provide such a tutorial. To make this discussion
concrete I will develop a very simple KDE application adding such details
as `Drag and Drop', URL support and online help. This document does not
try to explain C++ or basic compilation issues, it is only concerned with
how to put a GUI together.

<P>To begin with we'll just create a very simple GUI consisting only of
a menubar containing a single menu. The only item in the menu will be the
`Exit' command. From this rather humble beginning we'll extend our
application into a simple but functional text editor. This may seem like
a rather formidable task for a tutorial, but as you will see by
reusing the existing components provided by Qt and KDE it is made very
simple in practice.&nbsp; I'll give the code first then go through it
in detail afterwards. All of the example code is available free for
download from the KDE development website at
<A HREF="http://developer.kde.org/">http://developer.kde.org/examples.tar.gz</A>.
<BR>&nbsp;
<H2>
A Minimal Application</H2>
We'll now examine in detail a minimal KDE Application. The application
only provides a single function `Exit', but already supports floating
menubars, session management and internationalisation.
<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR NOSAVE>
<TD NOSAVE>
<PRE>
#ifndef EDIT_H
#define EDIT_H

#include &lt;ktopwidget.h&gt;

class Edit : public KTopLevelWidget
{
&nbsp; Q_OBJECT

public:
&nbsp; Edit();

&nbsp; ~Edit();

public slots:
&nbsp;void commandCallback(int id_);

private:
&nbsp; // Child widgets
&nbsp; KMenuBar *menu;
};
#endif // EDIT_H
</PRE>
</TD>
</TR>

<CAPTION ALIGN=BOTTOM><B>Edit</B> class declaration&nbsp;
<BR>&nbsp;</CAPTION>
</TABLE>
&nbsp; The first thing to look at is the class declaration, as you can
see our application is implemented as the class <B>Edit</B>. <B>Edit</B>
is a subclass of <B>KTopLevelWidget</B> (KTLW) which is the baseclass of
all KDE applications. KTLW manages an applications menu bar, status bar
and toolbars, and provides support for session management. The next thing
in the class declaration is the `Q_OBJECT' macro, this is needed in order
to support the important Qt features of `signals' and `slots'. The only
slot in the <B>Edit</B> class is the <I>commandCallback()</I> method, it
has been declared as a slot with `public' access by the line that reads
`<TT>public slots:'.</TT> A slot is essentially a sort of typesafe callback
method, it will be called by objects that the <B>Edit</B> class has expressed
an interest in using the <I>connect()</I> method. The rest of the class declaration
is like any other C++ class.
<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
#include &lt;kapp.h&gt;
#include &lt;kmenubar.h&gt;
#include &quot;edit.h&quot;

const int ID_EXIT= 111;

Edit::Edit()
{
&nbsp; QPopupMenu *file = new QPopupMenu;

&nbsp; file->insertItem(klocale->translate(&quot;Exit&quot;), ID_EXIT);

&nbsp; menu = new KMenuBar( this );
&nbsp; CHECK_PTR( menu );
&nbsp; menu->insertItem( klocale->translate(&quot;File&quot;), file );
&nbsp; menu->show();
&nbsp; setMenu(menu);

&nbsp; // Connect things together
&nbsp; connect (file, SIGNAL (activated (int)), SLOT (commandCallback (int)));
}

Edit::~Edit()
{

}

void Edit::commandCallback(int id_)
{
&nbsp; switch(id_) {
&nbsp; case ID_EXIT:
&nbsp;   exit(0);
&nbsp;   break;
&nbsp; }
}

#include &quot;edit.moc&quot;
</PRE>
</TD>
</TR>

<CAPTION ALIGN=BOTTOM><B>Edit</B> class implementation</CAPTION>
</TABLE>


<P>Now we'll study the implementation of our <B>Edit</B> class,
looking first at the constructor. The constructor creates a QPopupMenu
object, this is the File menu. We add the `Exit' item using the
<I>insertItem()</I> method. The first parameter is the string to be
displayed in the menu, the second is an integer id (you can find out
about all the methods of a class by looking at it's class
documentation). In order to prepare the applet for translation we call
<I>klocale->translate()</I> rather than just setting the label to the
literal string 'Exit', this should be done for any string the user
will see. We don't have to create our own KLocale object as
KApplication (the base class of all KDE application objects) creates
one for us. We access the KLocate object using the klocale macro
provided by kapp.h.

<P>
The next thing we need to do is create a menubar to contain our menu,
we do this using a KMenuBar widget. The CHECK_PTR macro used to ensure
the widget was created successfully is a Qt provided macro that checks
it's parameter is not 0 (if the parameter is 0 it terminates the
application with an error message). We create the 'File' item in
almost the same way as we created the menu entry (the similarity
should come as no suprise once you know that QPopupMenu and KMenuBar
have a common baseclass QMenuData). Again we need to call KLocale as
string 'File' will be visible to the user. We display our menu by
calling its <I>show()</I> method then tell KTLW to manage our menu
with <I>setMenu()</I>.

<P>
The final stage in making our applet work is to connect the menubar
entry to some code to exit the application, that's what the
<I>connect()</I> call does. Qt applications (and hence KDE
applications) use signals and slots to wire one widget to another. A
widget (or any other QObject) will emit a signal whenever it's state
changes, our applet uses the <I>activated()</I> signal that a
QPopupMenu emits when a user selects a menu item. This signal must be
connected to the <I>commandCallback()</I> slot we declared in the
<B>Edit</B> class. As you can see the <I>commandCallback()</I> slot
looks no different to a normal method, the only difference is the
declaration that this method is a slot in the header file.
<I>commandCallback()</I> exits the program if the id_ parameter it was
passed is the constand ID_EXIT - the same constant we used to identify
the 'Exit' menu item.  Signals and slots can have parameters and they
are treated like those of any other method. Qt checks that the type
signature of the signal matches that of the slot (and reports the
error if they do not match). Now we can take a look at the
<I>connect()</I> method call in the Edit constructor.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
&nbsp; connect (file, SIGNAL (activated (int)), SLOT (commandCallback (int)));
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Connecting a signal to a slot</CAPTION>
</TABLE>
<P>

The first parameter is the object that will omit the signal, in our
case it's the QPopupMenu file. The next parameter is the signal to be
connected, you must specify the name of the signal and the types of
it's parameters (as they appear in the class header file or class
documentation) using the <I>SIGNAL()</I> macro. The last parameter
<I>connect()</I> takes is the slot specified using the <I>SLOT()</I>
macro and specifying it's parameters as before. Qt will inform you at
run time if the connection is invalid.  QObject provides a number of
other overloaded versions of the <I>connect()</I> method, you should
look at the Qt class documentation for more details. It is perfectly
acceptable to connect a signal to more than one slot or to connect
more than one signal to a single slot.
<P>

The last feature to note about this simple applet is that we include
the file edit.moc at the end of the class implementation. The `moc'
file is the output of the moc preprocessor that must must be applied
to any class that inherits QObject. You normally invoke moc via a
makefile rather than directly and you must ensure that its output is
compiled and linked to your application. The easiest way to ensure
this is to simply include the moc output in the class implementation
which is the approach used here. It is important to ensure that moc is
rerun if you change the class declaration or you may get strange
errors.
<P>

You may think that this was a lot of work just to create a completely
useless applet, but we've come a long way already: We've learned about
session management, creating menus, internationalisation and the basis
of Qt, signals and slots. The next stage will be to make our applet
actually do something!

<H2>Adding functionality</H2>

Now that we know the basics we can move on a bit more quickly, so I
won't be covering the code in quite as much detail as in the last
section. Our aim now is to make the applet do something, in our case
we want to be able to edit text.

<P>
You can see the complete code for this version of <B>Edit</B> as
example2 in the source files for this tutorial.

<P>
<CENTER><IMG SRC="example2.gif"></CENTER>

<P>
Qt provides us with QMultiLineEdit, a widget for editing text, this
will form the core of our editor applet. We declare a member variable
to hold the widget in the <B>Edit</B> class declaration like this:

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
&nbsp; QMultiLineEdit *view;
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Declaring the editor widget</CAPTION>
</TABLE>
All we need to do now is create the widget in <B>Edit</B>s constructor
and tell
KTLW to manage it.
<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD><PRE>
&nbsp; view= new QMultiLineEdit(this, "Main View");
&nbsp; setView(view);
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Creating the editor widget</CAPTION>
</TABLE>
<P>
We already know how to add menu items, so adding some new ones for
load and save is easy. The code is almost the same as that for the
'Exit' item (note that we now need some more constants like ID_SAVE to
identify the new commands).
<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
&nbsp; file->insertItem(klocale->translate("Open..."), ID_OPEN );
&nbsp; file->insertItem(klocale->translate("Save"), ID_SAVE);
&nbsp; file->insertItem(klocale->translate("Save As..."), ID_SAVEAS);
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Adding some more menu items</CAPTION>
</TABLE>
Notice that these entries all end in an ellipsis ('...'), this
indicates that selecting this item will cause a dialog to be displayed
for the user to give more information. Using an ellipsis in this way
is almost universal across all GUIs and you should follow this
convention religiously.
<P>
Now that we've created the menu items, all that remains is to make
them do something. The code required is very straight forward. We'll
look first at the `Open' command. All we need to do is create a new
case in the switch statement of the <I>commandCallback()</I> slot, and
then add the code to open a file.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
&nbsp; case ID_OPEN:
&nbsp;   name= QFileDialog::getOpenFileName();
&nbsp;   if (!name.isEmpty()) {
&nbsp;     load(name);
&nbsp;   }
&nbsp;   break;
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Opening a file in <B>Edit</B></CAPTION>
</TABLE>

The new code makes use of a static method of the QFileDialog class to
pop up a modal file dialog and get a filename. If the user pressed the
`Cancel' button then the empty string is returned so we test for this
case and do nothing. If the user selected a filename then we read the
file into the editor widget by calling the load method.

<P>
The load method uses the QFile class to read the contents of the
selected file. The QFile class is provided by Qt and provides an
easy to use platform independent way to access files. It is used
here in combination with QTestStream which lets us read the contents
of the file a line at time. Each line is appended to the text widget
as it is read.

It is important to keep track of the name of the current file name
(so we can implement `Save') so we add a new instance variable
<I>filename_</I> to the Edit class. This value is set by the load
method.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
  void Edit::load(const char *filename) {
      QFile f( filename );
      if ( !f.open( IO_ReadOnly ) )
	  return;

      view->setAutoUpdate( FALSE );
      view->clear();

      QTextStream t(&f);
      while ( !t.eof() ) {
  	QString s = t.readLine();
	view->append( s );
      }
      f.close();

      view->setAutoUpdate( TRUE );
      view->repaint();
      filename_= filename;
  }
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>The implementation of the <B>load()</B> method.
</CAPTION>
</TABLE>

One feature of this method to note is the use of setAutoUpdate() to
prevent flickering and improve performance. The effect is to disable
redraws for the text widget until the entire file is read in - otherwise
the widget would be repainted for each line. It is very important not
to leave a widget in this state, so when we have finished reading the
file we call setAutoUpdate() again to re-enable updates. We also call
repaint() to ensure the widget is properly redrawn.
<P>

The 'Save As' command is almost the same as the 'Open' command. We now
use the <I>getSaveFileName()</I> which unlike <I>getOpenFileName()</I>
does not require that the filename choosen should be that of an
existing file. The save command is also easy: if we already know the
filename then save the text, if not then call the <I>saveAs()</I>
command.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
    case ID_SAVE:
      if (!filename_.isEmpty())
        saveAs(filename_);
      else {
        name= QFileDialog::getSaveFileName();
        if (!name.isEmpty())
          saveAs(name);
      }
      break;
    case ID_SAVEAS:
      name= QFileDialog::getSaveFileName();
      if (!name.isEmpty())
        saveAs(name);
      break;
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Saving a file in <B>Edit</B></CAPTION>
</TABLE>

The code to actually save the file is simple, we use QFile as before,
but this time we open the file with the <I>IO_WriteOnly</I> flag to
indicate we want to open the file for output. We write the whole text
as a single (multiline) string obtained with the text() method of
QMultiLineEdit;

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
  void Edit::saveAs(const char *filename) {
      QFile f( filename );
      if ( !f.open( IO_WriteOnly ) )
	  return;

      QTextStream t(&f);
      t &lt;&lt; view->text();
      f.close();

      setHint(filename);
      filename_= filename;
  }
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>The implementation of the <B>saveAs()</B> method.
</CAPTION>
</TABLE>

The last feature of this version of the example is an `About' box. This
is a dialog box usually accessed using the `About...' entry in the `Help'
menu. This is implemented here by calling the static method
QMessageBox::about(). `About' boxes are usually used to display information
such as the version number, author and vendor of an application.

<P>
<CENTER><IMG SRC="about.gif"></CENTER>
<P>

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
    case ID_ABOUT:
      QMessageBox::about(this, "About Edit",
	  	         "This is a simple text editor example program.");
      break;
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>The implementation of the <B>saveAs()</B> method.
</CAPTION>
</TABLE>

<H2>Improving the user interface</H2>

 Our applet is now capable of performing useful tasks, but its user
interface is rather poor. There are a number of deficiencies such as
the lack of a warning if you exit without saving changes, the lack of
any display of the name of the currently open file, and a lack of
keyboard shortcuts. We'll now try to rectify some of these defects.
<P>

<CENTER><IMG SRC="example3.gif"></CENTER>

<P>

The first thing we'll tackle is adding a status bar to our applet,
we'll use it to display the name of the currently open file. KDE
provides a status bar widget, KStatusBar, and KTLW knows how to manage
it, so there's not much left for us to do. KStatusBar has an API very
similar to that of the menu widgets so you should be able to figure
out what's going on. Note that after showing our status bar we tell
KTLW to manage it. We can change the string displayed on the status
bar using the id constant ID_HINTTEXT, this is implemented in the slot
<I>setHint()</I>.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
  void Edit::initStatusBar()
  {
    statusbar= new KStatusBar(this);
    statusbar->insertItem("Welcome to Edit", ID_HINTTEXT);
    statusbar->show();
    setStatusBar(statusbar);
  }

  void Edit::setHint(const char *text)
  {
    statusbar->changeItem(text, ID_HINTTEXT);
  }

</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Adding a status bar.
</CAPTION>
</TABLE>

<P>
We set the string in the status bar when the user invokes any method
that changes the filename, for example loading and saving.

<P>
We'll now add a toolbar to Edit, this is slightly more complicated as
we need to assign icons to the buttons and setup the tooltips. We make
use of the KIconLoader class to handle loading (and caching) of the toolbar
icons. This class searches in the directories specified by the KDE File
System Standard for an icon, which as we're only using standard icons, suits
us fine. We use a macro provided by <I>kapp.h</I> to access the icon loader
created by KApplication.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
  void Edit::initToolBar()
  {
    KIconLoader *loader = kapp->getIconLoader();
    toolbar = new KToolBar(this);

    toolbar->insertButton(loader->loadIcon("filenew.xpm"),
			  ID_NEW, TRUE,
			  klocale->translate("Create a new file"));
    toolbar->insertButton(loader->loadIcon("fileopen.xpm"),
	 		  ID_OPEN, FALSE,
			  klocale->translate("Open a file"));
    toolbar->insertButton(loader->loadIcon("filefloppy.xpm"),
			  ID_SAVE, FALSE,
			  klocale->translate("Save the current file"));

    addToolBar(toolbar);
    toolbar->setBarPos(KToolBar::Top);
    toolbar->show();
    connect(toolbar, SIGNAL(clicked(int)), SLOT(commandCallback(int)));
  }

</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Adding a toolbar.
</CAPTION>
</TABLE>

Each toolbar item is created by a call to insertButton(). This method
takes the icon (accessed using KIconLoader), the command id, a flag to
indicated if the button is enabled and the text for the tooltip as
parameters. As usual we allow for i18n support and wrap the tooltip in
a call to klocale->translate().

<P>
We connect the toolbars clicked signal to the same slot as we use for
the menus. This is perfectly safe as we are using the same id numbers
for commands in each, and we are free to connect as many signals as we
want to a slot.

<P>
It is generally good practice when designing a user interface to prevent
the user invoking an operation that cannot be performed. We do this in Qt
by enabling and disabling widgets depending on the state of the application.
For example if you have made no changes to a document then saving to its
original file makes no sense (though saving it to a different file is
often useful). All Qt and KDE widgets have a method <I>setEnabled()</I>
which is used to `grey out' the widget and disable it.

<P>
KMenuBar and KToolbar both make the process of enabling and disabling
their entries easy by providing a method which will set the state of
an entry specified by its command id. We call both of these in the
implementation of enableCommand().

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
  void Edit::enableCommand(int id) //SLOT
  {
    toolbar->setItemEnabled(id, true);
    menu->setItemEnabled(id, true);
  }

  void Edit::disableCommand(int id) // SLOT
  {
    toolbar->setItemEnabled(id, false);
    menu->setItemEnabled(id, false);
  }
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>Enabling and disabling commands.
</CAPTION>
</TABLE>

We shall now use these methods to implement context sensitive enabling
and disabling of commands. To make the `Save' command disabled when there
are no changes to save we need to keep track of when the text is modified,
this is dome using the textChanged() signal emitted by QMultiLineEdit when
the user edits the text. Using this signal we can create a flag to indicate
if the text has been changed. This is handled by the textChanged() slot
defined by the <B>Edit</B> class. This slot also updates the state of the
commands.

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
void Edit::textChanged()
{
  modified= true;
  enableCommand(ID_SAVE);
  enableCommand(ID_SAVEAS);
}
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>The definition of the <I>textChanged()</I> slot.
</CAPTION>
</TABLE>

Note that the load and save methods must also be modified to keep the
flag up to date, and to enable or disable commands. You can see these
changes for yourself by looking at the example programs.

<P>
The final thing we will add to our editor in this section is a confirmation
dialog to be shown when the user tries to exit when there are unsaved
changes in the editor. The dialog is a standard Qt warning dialog.

<P>
<CENTER><IMG SRC="exit-yesno.gif"></CENTER>
<P>

The dialog is implemented using the exit() method. This method simply
tests the flag we created to indicate modifications, and uses another
static method of QMessageBox to display a warning dialog.
<P>

<BR>&nbsp;
<TABLE BORDER COLS=1 WIDTH="90%" NOSAVE >
<TR>
<TD>
<PRE>
  int Edit::exit()
  {
    int die= 0;

    if (!modified)
      die= 1;
    else
      if (QMessageBox::warning(this, klocale->translate("Unsaved Changes"),
  			       "You have unsaved changes, you will loose "
			       "them if you exit now.",
			       "Exit", "Cancel",
			       0, 1, 1))
         die= 0;
      else
        die= 1;

    return die;
  }
</PRE>
</TD>
</TR>
<CAPTION ALIGN=BOTTOM>The definition of the <I>exit()</I> method.
</CAPTION>
</TABLE>

<H2>Forthcoming...</H2>
<UL>
<LI>Adding kdehelp support
<LI>QToolTipGroup
<LI>Using KConfig
<LI>Drag and drop
<LI>Adding network file access
</UL>

<HR>
<SMALL>This document is Copyright Richard J. Moore 1997-1998 (rich@kde.org)
<BR>
You are free to distribute the examples given here in any form what so
ever, and under any license terms. You are however NOT free to do the
same with the text of the tutorial itself. You are permitted to
distribute the text of the tutorial in electronic formats as long as
it is distributed in its entirity. You may not charge for this
document, though you may recoup packaging costs. You may make paper
copies for personal use only.  If you want to distribute this under
other terms then drop me a mail.
</SMALL>
